<?php

declare(strict_types=1);

namespace App\Utils;

class DefaultCircularReferenceHandler
{
    /**
     * @param mixed $object
     * @return mixed
     */
    public function __invoke($object)
    {
        return $object->getId();
    }
}
